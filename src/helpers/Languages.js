export default {
  exit: 'Bỏ qua',
  signIn: 'Đăng Nhập',
  signUp: 'Đăng ký',
  signUpDescription: 'Bạn chưa có tài khoản?',
  phone: 'Số điện thoại',
  pass: 'Mật khẩu',
  errPhone: 'Nhập số điện thoại!',
  errPass:  'Nhập số mật khẩu!',
  notif: 'Thông báo',
  messenger: 'Phản ánh',
  utility: 'Tiện ích',
  Account: 'Tôi',
};
