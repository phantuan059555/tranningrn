import React, {Component} from 'react';
import {StyleSheet, Text, StatusBar, View, SafeAreaView} from 'react-native';
import {Scale} from '../helpers';
import {Actions} from 'react-native-router-flux';
class SplashScreen extends Component {
  performTimeConsumingTask = async () => {
    return new Promise((resolve) =>
      setTimeout(() => {
        resolve('result');
      }, 2000),
    );
  };

  componentDidMount = async () => {
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      Actions.onBoarding();
    }
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="dark-content" hidden={true} backgroundColor='transparent' translucent />
        <View style={styles.vHeader}>
          <Text>PAT</Text>
        </View>
        <View style={styles.vContent}>
          <View style={styles.img} />
        </View>
        <View style={styles.vFooter} />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3443be',
    justifyContent: 'center',
    alignContent: 'center',
  },
  vHeader: {
    width: Scale.moderateScale(279),
    height: Scale.moderateScale(299),
    backgroundColor: 'red',
  },
  vContent: {
    width: '100%',
    height: Scale.moderateScale(106),
    marginVertical: Scale.moderateScale(54),
    justifyContent: 'center',
    alignItems: 'center',
  },
  vFooter: {
    width: '100%',
    height: Scale.moderateScale(247),
    backgroundColor: 'green',
  },
  img: {
    width: Scale.moderateScale(106),
    height: Scale.moderateScale(106),
    backgroundColor: '#fff',
  },
});

export default SplashScreen;
