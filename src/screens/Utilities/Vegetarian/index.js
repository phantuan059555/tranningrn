import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList, TouchableOpacity } from 'react-native';
import { size } from 'lodash';
import ItemVegetarian from './ItemVegetarian';
import { Scale } from '../../../helpers';
class VegetarianFood extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
                {
                    id: 1,
                    img: 'https://raw.githubusercontent.com/Jacse/react-native-app-intro-slider/master/examples/assets/1.jpg',
                    title: 'Đặt bàn miễn phí, ưu đãi mê ly',
                    phone: '0339382899'
                },
                {
                    id: 2,
                    img: 'https://raw.githubusercontent.com/Jacse/react-native-app-intro-slider/master/examples/assets/1.jpg',
                    title: 'Giảm 5%',
                    phone: '0339382899'
                },
            ]
        };
    }
    _renderItem = ({ item, index }) => {
        return (
            <ItemVegetarian item={item} index={index} />
        )
    }
    _keyExtractor = index => index.toString();

    render() {
        const { data } = this.state;
        return (
            <View style={styles.conatiner}>
                <View style={styles.title}>
                    <Text style={styles.nameFood}>Đồ chay</Text>
                    <TouchableOpacity>
                        <Text style={styles.viewALL}>xem tất cả</Text>
                    </TouchableOpacity>
                </View>
                {size(data) > 0 ? (
                    <FlatList
                        style={styles.flatStyle}
                        horizontal
                        data={data}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    />
                ) : (
                        <></>
                    )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    conatiner: {
        flex: 1,
        backgroundColor: '#fff',
    },
    title: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: Scale.moderateScale(16),
    },
    nameFood: {
        fontSize: 17,
        color: '#000',
        fontWeight: 'bold',
    },
    viewALL: {
        fontSize: 14,
        color: 'blue',
        paddingRight: Scale.moderateScale(18),
    },
    flatStyle: {
        flex: 1,
    }
})
export default VegetarianFood;