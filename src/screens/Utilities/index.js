import React, { Component } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import DrinkFood from './DrinkFood/index';
import EatFood from './EatFood/index';
import VegetarianFood from './Vegetarian/index';
import { Scale } from '../../helpers';
class Utilities extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={styles.main}>
                <ScrollView style={{ flex: 1, marginBottom: 8 }}>
                    <EatFood />
                    <DrinkFood />
                    <VegetarianFood />
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex: 1,
        marginLeft: Scale.moderateScale(16),
    }
})

export default Utilities;