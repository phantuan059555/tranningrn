import React, { Component } from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';
import { Scale } from '../../../helpers';
import Icons from 'react-native-vector-icons/Entypo';
const ItemEatFD = ({item, index}) => {
    return (
        <View style={styles.wapper}>
          <TouchableOpacity>
              <Image style={styles.img} source={{uri: item.img}} />
              <Text style={styles.title} numberOfLines={1}>{item.title}</Text>
              <View style={styles.wapperPhone}>
                <Icons name="phone" size={14} color="#f26d24" />
                <Text style={styles.phone}>{item.phone}</Text>
              </View>
          </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    wapper: {
        flexDirection: 'column',
        marginRight: Scale.moderateScale(10),
    },
    img: {
        width: Scale.moderateScale(228),
        height: Scale.moderateScale(153),
        borderRadius: Scale.moderateScale(8),
    },
    title: {
        fontSize: 14,
        color: '#000',
        paddingVertical: Scale.moderateScale(10),
    },
    wapperPhone: {
        flexDirection: 'row'
    },
    phone: {
        fontSize: 14,
        color: '#f26d24',
        paddingLeft: 4,
    }
})
export default ItemEatFD;