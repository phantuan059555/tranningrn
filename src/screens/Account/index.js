import React, { Component } from 'react';
import {StyleSheet, View, Text} from 'react-native';

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.main}>
                <Text>Account</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default Account;