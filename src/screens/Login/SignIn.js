import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native';
import Container from '../../components/container';
import { Scale, Languages } from '../../helpers';
import { Input } from '../../components';
import { connect } from 'react-redux';
import { loginApp} from '../../redux/middlewares/LoginMiddleware';
import Loading from '../../components/Loading';
import {Actions} from 'react-native-router-flux';
class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: '',
      pass: '',
      errPhone: '',
      errPass: ''
    };
  }

  onchangePhone = value => {
    this.setState({
      phone: value,
      errPhone: '',
    })
  }

  onchangePass = value => {
    this.setState({
      pass: value,
      errPass: '',
    })
  }

  loginSubmit = () => {
      const {phone, pass} = this.state;
      const {loginApp} = this.props;
      if(phone == '') return this.setState({errPhone: `${Languages.errPhone}`})
      if(pass == '') return this.setState({errPass: `${Languages.errPass}`})
      return loginApp(phone,pass);
  };

  _navigationHome = () => {
    Actions.bottomTab();
  }

  render() {
    const { phone, pass, errPhone, errPass } = this.state;
    const {isLoading} = this.props;
    return (
      <Container styleContent={{ backgroundColor: '#f6f7fb' }}>
        <View style={styles.header}>
          <View style={styles.logo}>
            <View style={{ width: 40, height: 40, backgroundColor: '#fff' }} />
            <Text style={{ fontSize: 24, fontWeight: 'bold', paddingTop: 4, color: '#fff' }}>BMS</Text>
          </View>
          <View style={styles.form}>
            <View style={styles.viewTxt}>
              <Text style={styles.txtSignIn}>{Languages.signIn}</Text>
              <Input 
              ref={ref => (this.refPhone = ref)}
              title={`${Languages.phone}`} 
              value={phone} 
              onChangeText={this.onchangePhone} 
              keyboardType="phone-pad" 
              errText={errPhone}
              returnKeyType="next"
              />
              <Input 
              title={`${Languages.pass}`} 
              value={pass} 
              onChangeText={this.onchangePass} 
              secureTextEntry 
              errText={errPass}
              returnKeyType="go"
              />
              <TouchableOpacity onPress={this.loginSubmit} style={styles.btnLogin}>
                <Text style={styles.txtBtn}>{Languages.signIn}</Text>
              </TouchableOpacity>
              <View style={styles.description}>
                <Text style={[styles.txtDescription, { color: '#000' }]}>{Languages.signUpDescription}</Text>
                <TouchableOpacity onPress={this._navigationHome}>
                  <Text style={[styles.txtDescription, { color: '#f26d24', paddingLeft: 4 }]}>{Languages.signUp}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <Loading isLoading={isLoading} title="...Loading"/>
      </Container>
  );
  }
}

mapStateToProps = state => {
  return {
    isLoading: state.LoginReducer.isLoading,
    error: state.LoginReducer.error,
  }
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: Scale.moderateScale(271),
    backgroundColor: '#f26d24',
  },
  logo: {
    width: Scale.horizontalScale(120),
    height: Scale.horizontalScale(120),
    marginBottom: Scale.moderateScale(16),
    marginTop: Scale.moderateScale(36),
    marginLeft: Scale.moderateScale(123),
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    width: Scale.moderateScale(345),
    height: Scale.moderateScale(333),
    marginHorizontal: Scale.moderateScale(15),
    borderRadius: Scale.moderateScale(16),
    backgroundColor: '#fff',
    flexDirection: 'column',
    shadowColor: '#000000',
    shadowOpacity: 0.2,
  },
  viewTxt: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtSignIn: {
    fontSize: Scale.moderateScale(17),
    color: '#000',
    paddingVertical: Scale.moderateScale(24),
  },
  btnLogin: {
    width: Scale.moderateScale(313),
    height: Scale.moderateScale(40),
    marginHorizontal: Scale.moderateScale(16),
    borderRadius: Scale.moderateScale(20),
    backgroundColor: '#f26d24',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtBtn: {
    fontSize: Scale.moderateScale(17),
    color: '#fff',
    fontWeight: 'bold'
  },
  description: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: Scale.moderateScale(24),
  },
  txtDescription: {
    fontSize: Scale.moderateScale(14),
  }
});

export default connect(mapStateToProps, {loginApp})(SignIn);
