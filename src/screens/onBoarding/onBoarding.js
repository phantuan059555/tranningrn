import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import {Values, Scale, Languages} from '../../helpers';
import Container from '../../components/container';
import {size} from 'lodash';
import SlideIntro from 'react-native-app-intro-slider';
import Icon from 'react-native-vector-icons/Ionicons';
import {Actions} from 'react-native-router-flux';

class onBoarding extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _renderIntro = ({item, index}) => {
    return (
      <View style={styles.wipper}>
        <Image style={styles.imgSw} source={{uri: item.img}} />
        <Text styles={styles.titleStyle}>{item.title}</Text>
      </View>
    );
  };

  _keyExtractor = (index) => {
    return index.toString();
  };

  _renderNextButton = () => {
    return (
      <View style={styles.viewNext}>
        {/* <Icon name="arrow-forward-outline" color="red" size={24} /> */}
      </View>
    );
  };

  _navigation = () => {
    Actions.SignIn();
  };

  render() {
    return (
      <Container styleContent={styles.container}>
        <View style={styles.vText}>
          <View />
          <View>
            <TouchableOpacity onPress={this._navigation}>
              <Text style={styles.textExit}>{Languages.exit}</Text>
            </TouchableOpacity>
          </View>
        </View>
        <SlideIntro
          data={Values.dataIntro}
          renderItem={this._renderIntro}
          renderNextButton={this._renderNextButton}
          dotStyle={{backgroundColor: 'blue'}}
          keyExtractor={this._keyExtractor}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    
  },
  vText: {
    marginRight: Scale.moderateScale(20),
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: Scale.moderateScale(16),
  },
  textExit: {
    fontSize: 16,
    color: 'red',
  },
  wipper: {
    flex: 1,
    flexDirection: 'column',
    marginHorizontal: Scale.moderateScale(45),
    marginBottom: Scale.moderateScale(65),
  },
  imgSw: {
    width: Scale.moderateScale(285),
    height: Scale.moderateScale(382),
    marginTop: Scale.moderateScale(60),
    marginBottom: Scale.moderateScale(40),
  },
  viewNext: {
    width: Scale.moderateScale(48),
    height: Scale.moderateScale(48),
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: Scale.moderateScale(20),
    marginRight: Scale.moderateScale(45),
  },
  titleStyle: {
    fontSize: 24,
    color: '#000',
  },
});

export default onBoarding;
