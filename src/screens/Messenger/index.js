import React, { Component } from 'react';
import {StyleSheet, View, Text} from 'react-native';

class Messenger extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.main}>
                <Text>Messenger</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default Messenger;