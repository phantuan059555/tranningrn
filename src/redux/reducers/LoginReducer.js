import {Login} from '../action/type';

const initState = {
    isLoading: false,
    error: false,
    user: null
}

const LoginReducer = (state = initState, action) => {
    switch (action.type) {
        case Login.FetchStart:
            return {
                ...state,
                isLoading: true,
                error: false,
            }
        case Login.FetchStart:
            return {
                ...state,
                user: action.data
            }
        case Login.FetchErr: 
        return {
            error: true,
            isLoading: false,
            user: null
        }
        default:
            return state;
    }
}

export default LoginReducer;