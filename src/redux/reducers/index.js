import {combineReducers} from 'redux';
import LoginReducer from './LoginReducer';

const rootReducers = combineReducers({
    LoginReducer,
});

export default rootReducers;
