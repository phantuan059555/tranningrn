import {fetchStart,fetchsucces,fetchErr} from '../action/LoginAction';
import {LoginService} from '../../services/LoginService';

const loginApp = (phone, pass) => dispatch => {
    dispatch(fetchStart())
    LoginService.login(phone,pass).then(res => {
      if (res) {
          console.log('login success')
          dispatch(fetchsucces(res));
          return true
      } else {
          console.log('login fail')
          return dispatch(fetchErr())
      }
    }).catch (error => {
        console.log(error);
    })
}

export {loginApp};