import {Login} from '../action/type';

const fetchStart = () => {
    return {
        type: Login.FetchStart
    }
}

const fetchsucces = data => {
    return {
        type: Login.FetchSucces,
        data
    }
}

const fetchErr = () => {
    return {
        type: Login.FetchErr
    }
}

export {fetchStart, fetchsucces, fetchErr};