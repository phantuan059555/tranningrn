import React, {Component} from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import {Color} from '../../helpers';
class Container extends Component {
  render() {
    const {bgStatus, styleContent} = this.props;
    return (
      <View style={[styles.container, {...styleContent}]}>
        <StatusBar
          backgroundColor={'transparent' | bgStatus}
          barStyle="dark-content"
          translucent
        />
        {this.props.children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundAPP,
  },
});

export default Container;
