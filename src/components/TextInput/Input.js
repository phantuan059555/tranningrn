import React, { PureComponent } from 'react';
import { StyleSheet, View, TextInput, Animated, Text } from 'react-native';
import PropTypes from 'prop-types';
import { Scale } from '../../helpers';

class Input extends PureComponent {
    static defaultProps = {
        titleActiveSize: 13,
        titleInActiveSize: 15,
        titleActiveColor: 'black',
        titleInactiveColor: 'dimgrey',
    }

    constructor(props) {
        super(props);
        const { value } = this.props;
        this.position = new Animated.Value(value ? 1 : 0);
        this.state = {
            isFieldActive: false,
        }
    }

    _handleFocus = () => {
        if (!this.state.isFieldActive) {
            this.setState({ isFieldActive: true });
            Animated.timing(this.position, {
                toValue: 1,
                duration: 150,
            }).start();
        }
    }

    _handleBlur = () => {
        if (this.state.isFieldActive && !this.props.value) {
            this.setState({ isFieldActive: false });
            Animated.timing(this.position, {
                toValue: 0,
                duration: 150,
            }).start();
        }
    }

    _returnAnimatedTitleStyles = () => {
        const { isFieldActive } = this.state;
        const {
            titleActiveColor, titleInactiveColor, titleActiveSize, titleInActiveSize
        } = this.props;

        return {
            top: this.position.interpolate({
                inputRange: [0, 1],
                outputRange: [14, 0],
            }),
            fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
            color: isFieldActive ? titleActiveColor : titleInactiveColor,
        }
    }

    render() {
        const { styleInut, placeholder, value, title, errText} = this.props;
        return (
            <View style={[styles.container, { styleInut }]}>
                <Animated.Text
                    style={[styles.titleStyles, this._returnAnimatedTitleStyles()]}
                >
                    {title}
                </Animated.Text>
                <TextInput
                    style={styles.input}
                    placeholder={placeholder}
                    value={value}
                    underlineColorAndroid='transparent'
                    onFocus={this._handleFocus}
                    onBlur={this._handleBlur}
                    {...this.props} />
                    <View style={styles.line} />
                  {errText ? <Text style={styles.txtErr}>{errText}</Text> : <></>}
            </View>
        );
    }
}

TextInput.propTypes = {
    styleInut: PropTypes.any,
    placeholder: PropTypes.string,
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    errText: PropTypes.string,
}

const styles = StyleSheet.create({
    container: {
        width: Scale.moderateScale(313),
        height: Scale.moderateScale(56),
        marginHorizontal: Scale.moderateScale(16),
        // backgroundColor: 'red',
        marginBottom: Scale.moderateScale(24),
    },
    input: {
        width: Scale.horizontalScale(300),
        marginTop: Scale.moderateScale(16),
    },
    titleStyles: {
        position: 'absolute',
    },
    line: {
        width: Scale.moderateScale(313),
        height: 2,
        backgroundColor: '#eaedef',
        marginTop: Scale.moderateScale(14),
    },
    txtErr: {
        fontSize: 12,
        color: 'red',
        marginTop: 4,
    }
})

export default Input;