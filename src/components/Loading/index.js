import React, { Component } from 'react';
import {StyleSheet, View, ActivityIndicator, Text} from 'react-native';
import PropTypes from 'prop-types';
class Loading extends Component {
    render() {
        const {isLoading, backgroundColorChild, colorLoading, backgroundColor, title} = this.props;
        return isLoading ? (
            <View
              style={[
                styles.main,
                {backgroundColor: backgroundColor || '#00000070'},
              ]}>
              <View
                style={[
                  styles.content,
                  {backgroundColor: backgroundColorChild || '#ffffff'},
                ]}>
                <ActivityIndicator size="large" color={colorLoading || '#000'} />
                {title ? <Text style={styles.text}>{title}</Text> : <></>}
              </View>
            </View>
          ) : (
            <></>
          );
        }
      
    
}

Loading.propTypes = {
  title: PropTypes.string,
  isLoading: PropTypes.bool,
  backgroundColorChild: PropTypes.any,
  colorLoading: PropTypes.any,
}

const styles = StyleSheet.create({
 content: { 
     minWidth: 80, 
     height: 80, 
     borderRadius: 10, 
     justifyContent: 'center', 
     alignItems: 'center' 
    },
  main: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: { color: '#000', fontSize: 13, marginTop: 5, textAlign: 'center', paddingHorizontal: 10 },
});

export default Loading;