import React from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import SplashScreen from '../screens/SplashScreen';
import onBoarding from '../screens/onBoarding/onBoarding';
import SignIn from '../screens/Login/SignIn';

import BottomNavigation from './BottomNavigaion';

const AppRouter = () => (
  <Router>
    <Stack key="root">
      {/* <Scene key="SplashScreen" component={SplashScreen} hideNavBar={true} />
      <Scene key="onBoarding" component={onBoarding} hideNavBar={true} /> */}
      <Scene key="SignIn" component={SignIn} hideNavBar={true} />
      <Scene key="bottomTab" component={BottomNavigation} hideNavBar />
    </Stack>
  </Router>
);
export default AppRouter;
