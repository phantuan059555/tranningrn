import React from 'react';
import {Router, Scene} from 'react-native-router-flux';
import Utilities from '../../screens/Utilities/index';
import { Scale } from '../../helpers';
const utilityNav = () => (
    <Router>
        <Scene
         key="tabUtility"
         tabs
         activeTintColor="#fff"
         tabBarStyle={{ backgroundColor: '#f26d24' }}
         inactiveTintColor="black"
         labelStyle={{ 
             fontSize: Scale.moderateScale(14), 
             fontWeight: 'bold', 
             paddingTop: Scale.moderateScale(20),
            }}
         tabBarPosition="top"
         indicatorStyle={{
             backgroundColor: '#fff',
            }}
        >
            {/* Tiện ích ngoài */}
           <Scene key="external" tabBarLabel="Tiện ích ngoài">
                <Scene
                    key="Utilities_external"
                    component={Utilities}
                    hideNavBar={true}
                />
            </Scene>

            {/* Tiện ích nội bộ */}
            <Scene key="internal" tabBarLabel="Tiện ích nội bộ ">
                <Scene
                    key="Utilities_internal"
                    component={Utilities}
                    hideNavBar={true}
                />
            </Scene>
        </Scene>
    </Router>
)

export default utilityNav;