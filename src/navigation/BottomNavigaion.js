import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons'
import Notif from '../screens/Notif/Notif';
import Messenger from '../screens/Messenger/index';
import Account from '../screens/Account/index';
import utilityNav from './utilityNav/index';
import { Languages, Scale } from '../helpers';

const BottomNavigation = () => (
    <Router>
        <Scene
            key="bottomTab"
            tabs
            activeTintColor="#C6426E"
            tabBarStyle={{ backgroundColor: '#fff' }}
            inactiveTintColor="black"
            labelStyle={{ fontSize: 8, marginBottom: Scale.moderateScale(10) }}
            tabBarPosition="bottom"
            showIcon
        >
            {/* Tab Notif */}
            <Scene key="notif" tabBarLabel={`${Languages.notif}`}>
                <Scene
                    key="Notif"
                    component={Notif}
                    hideNavBar={true}
                />
            </Scene>
            {/* Tab Messenger */}
            <Scene key="tbMessenger" tabBarLabel={`${Languages.messenger}`}>
                <Scene
                    key="Messenger"
                    component={Messenger}
                    hideNavBar={true}
                />
            </Scene>
            {/* Tab Utilities */}
            <Scene key="utilities" tabBarLabel={`${Languages.utility}`}>
                <Scene
                    key="Utilities"
                    component={utilityNav}
                    hideNavBar={true}
                />
            </Scene>
            {/* Tab Account */}
            <Scene key="account" tabBarLabel={`${Languages.Account}`}>
                <Scene
                    key="Account"
                    component={Account}
                    hideNavBar={true}
                />
            </Scene>
        </Scene>
    </Router>
)

export default BottomNavigation;