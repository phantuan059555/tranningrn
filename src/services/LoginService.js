import {API} from '../configs';
import {Request} from '../configs/Request'
import DeviceInfo from 'react-native-device-info'
export const LoginService = {
    login:async (phone, pass) => {
        try {
            const params = {
                language: "vi",
                phone: phone,
                word: pass,
                deviceInfo: DeviceInfo.getDeviceId()
            }
         const res = await Request.POST(API.Login.login, params);
         console.log('data user', res);
         if (res) {
             return res.data
         }
         return null
        } catch (error) {
         console.log(error);
          return null;
        }
    }
}