import React from 'react';
import {StatusBar} from 'react-native';
import AppRouter from './src/navigation/Navigation';
import {Provider} from 'react-redux';
import {store} from './src/redux/store/index';

const App = () => {
  return (
    <>
    <Provider store={store}>
       <AppRouter />
    </Provider>
    </>
  );
};

export default App;
